//
//  HelloWorldLayer.h
//  AGP_Assignment
//
//  Created by Jakub Rogalski on 09/05/2012.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

// HelloWorldLayer
@interface HelloWorldLayer : CCLayer
{
    CCSprite *background;
    CCSprite *duck;
    float move;
    
    //is game playing
    bool playing;
    
    //falling objects
    NSMutableArray *active; // active objects  
    NSMutableArray *inactive; // inactive objects- should be removed
    
    //round info
    float roundTimer; //time in round
    float falloutTimer; //time to fallout
    float falloutInterval; //time to next fallout
    
    //tap to play label
    CCLabelTTF *startLabel;
    
    //score
    float score;
    CCLabelAtlas *scoreLabel;
    
    //is user touching the screen
    bool touching;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
