//
//  HelloWorldLayer.m
//  AGP_Assignment
//
//  Created by Jakub Rogalski on 09/04/2012.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"
#import "SimpleAudioEngine.h"

// HelloWorldLayer implementation
@implementation HelloWorldLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
        
    // enable touch
        self.isTouchEnabled = YES;
        
    // ask director for the window size
        CGSize size = [[CCDirector sharedDirector ] winSize];
		
	// create the background
        background = [CCSprite spriteWithFile:@"game_bcg.png"];
        background.position = ccp(size.width/2, size.height/2);
        [self addChild: background];
        
    // create the duck sprite
        duck = [CCSprite spriteWithFile:@"duck.png"];
        duck.position = ccp(size.width/2, 5 + duck.contentSize.height/2);
        [self addChild: duck];
        
    // initially hide the sprite
        duck.visible = NO;
        
        
    // create an array of objects
		active = [[NSMutableArray alloc] init];
		inactive = [[NSMutableArray alloc] init];
		
    // create and initialize a Label
		startLabel = [CCLabelTTF labelWithString:@"Tap to Play" fontName:@"STHeitiJ-Light" fontSize:56 ];
    // position the label on the center of the screen
		startLabel.position = ccp( size.width /2 , size.height/2 );
    
    // add the label as a child to this Layer
		[self addChild: startLabel];
        
        
    // scoring
        scoreLabel = [CCLabelAtlas labelWithString:@"0" charMapFile:@"led.png" itemWidth:15 itemHeight:32 startCharMap:'0'];
        scoreLabel.position = ccp(480 - 16 - scoreLabel.contentSize.width, 320 - 32);
        [self addChild: scoreLabel];
        
    // falling leaves effect
        CCParticleSystem *leaves = [CCParticleSystemPoint particleWithFile: @"leaves.plist"];
        [self addChild: leaves];
        
    //preload sounds
		[[SimpleAudioEngine sharedEngine] preloadEffect: @"blast.wav"];
		[[SimpleAudioEngine sharedEngine] preloadEffect: @"fallout.wav"];
        
    // play repeating duck sound as background music but set volume to 0
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:0];
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic: @"duck.wav"];
        
    // start accelerometer
        [[UIAccelerometer sharedAccelerometer] setDelegate:self];
        [[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / 30)];
        
    // schedule update loop
        [self schedule:@selector(update:)];
	}
	return self;
}

- (void) update : (ccTime) dt
{
    // add to our round and fallout timers
    roundTimer += dt;
    falloutTimer += dt;
    
    // playing logic (when duck is alive)
    if (playing)
    {
        // update score
        score += dt;
        [scoreLabel setString: [NSString stringWithFormat: @"%u", (unsigned int) (score * 1000)]];
        scoreLabel.position = ccp(480 - 16 - scoreLabel.contentSize.width, 320 - 32);
        
        // new round every 4 seconds
        if (roundTimer > 4)
        {
            // next round
            roundTimer = 0;
            falloutInterval -= 0.1;
            if (falloutInterval < 0.1) falloutInterval = 0.1;
        }
        
        // check for next fallout
        if (falloutTimer > falloutInterval)
        {
            // create new fallout
            falloutTimer = 0;
            CCSprite *fallout = [CCSprite spriteWithFile:@"fallout.png"];
            fallout.position = ccp(arc4random() % 480, 320 + fallout.contentSize.height/2);
            [self addChild: fallout];
            [active addObject: fallout];
        }
    }
    
    // create the duck rectangle
    CGRect rectDuck = CGRectMake(duck.position.x - duck.contentSize.width / 2.0, 
                                 duck.position.y - duck.contentSize.height / 2.0, 
                                 duck.contentSize.width, 
                                 duck.contentSize.height);
    
    // go through fallout and move them down the screen
    for (CCSprite *fallout in active)
    {
        fallout.position = ccp(fallout.position.x, fallout.position.y - dt * 320.0);
        fallout.rotation += 360.0 * dt;
        if (fallout.position.y < 20)
        {
            // add to inactive list
            [inactive addObject: fallout];
            
        }
    
        if (playing)
        {                   
            // create the fallout rectangle
            CGRect rectFallout = CGRectMake(fallout.position.x - 8, 
                                            fallout.position.y - 8, 
                                            16, 
                                            16);
            //on collision
            if (CGRectIntersectsRect(rectDuck, rectFallout))
            {
                // kill the duck
                duck.visible = NO;
                playing = NO;
                startLabel.visible = YES;
                
                [self createExplosion: @"blow.plist" inRect: rectDuck];
                
                // play duck explosion sound
                [[SimpleAudioEngine sharedEngine] playEffect:@"blast.wav"];
                
                // start duck sound at lowest volume
               [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:0.1];
            }
        }
    }

    
    
    // go through inactive and remove and explode
    for (CCSprite *fallout in inactive)
    {
        // create a fallout explosion
        CGRect rectFallout = CGRectMake(fallout.position.x - fallout.contentSize.width/2, 
                                        fallout.position.y - fallout.contentSize.height/2, 
                                        fallout.contentSize.width/2, 
                                        fallout.contentSize.height/2);
        [self createExplosion: @"fallout.plist" inRect: rectFallout ];
        
        // play splash sound
		[[SimpleAudioEngine sharedEngine] playEffect:@"fallout.wav"];
        
        // remove from active list and our scene
        [active removeObject: fallout];
        [self removeChild:fallout cleanup:YES];
    }
    
    // clear out inactive queue
    [inactive removeAllObjects];
    
    // if not playing we get out of loop here
    if (playing == false) return;
    
    // move duck sprite based on controls
    float x = duck.position.x;
    
    // move duck sprite that it takes 1 sec to get across entire screen
    x += move * dt * 480.0;
    
    // keep duck on the screen
    if (x < 80) x = 80;
    if (x > 400) x = 400;
    
    // update position of duck sprite
    duck.position = ccp(x,duck.position.y);
    
    // set duck sound to be a little louder when moving
    float volume = 0.7 + fabs(move);
    if (volume > 1) volume = 1;
    [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:volume];
}


- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{   
    touching = true;
    
    // check for game start
    if (playing == NO)
    {
        // wait until all objects are crashed
        if (active.count == 0)
        {
            // reset all vars
            falloutInterval = 0.5;
            startLabel.visible = NO;
            duck.visible = YES;
            //roundTimer used in order to make the game more difficult as it progresses
            roundTimer = 0;
            playing = YES;
            score = 0;
            
            // start duck sound at lower volume
            [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:0.7];
        }
        return;
    }
    
    // not using multitouch so can just grab any object
    UITouch *touch = [touches anyObject];
    
    // get location of touch
    CGPoint location = [touch locationInView:[touch view]];
    
    if (location.x < 64) move = -1;
    else if (location.x > 480-64) move = 1;
    else move = 0;
}
- (void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event 
{
}
- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event 
{	
    move = 0;
    touching = false;
    
}
- (void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    move = 0;
    touching = false;
    
}

- (void) createExplosion: (NSString*) name inRect: (CGRect) rect
{
	CCParticleSystem *explosion = [CCParticleSystemPoint particleWithFile: name];
	explosion.position = ccp(rect.origin.x+rect.size.width/2,rect.origin.y+rect.size.height/2);
	explosion.autoRemoveOnFinish = YES;
    [self addChild: explosion];
}

- (void)accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration
{   
    if (touching) return;
    
    // we are landscape mode so the y axis is really our x axis
    move = acceleration.y * 2.0;
    
    // if x is negative we are reversed landscape mode so flip y
    if (acceleration.x < 0) move = -move;
    
    // limit movement to range of -1 to 1
    if (move < -1) move = -1;
    if (move > 1) move = 1;
    
    // ignore smaller values to avoid unintended jitter of the sprite
    if (move > -0.1 && move < 0.1) move = 0;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    // turn off accelerometer
    [[UIAccelerometer sharedAccelerometer] setDelegate: nil];
    
    // unschedule this update loop
    [self unschedule: @selector(update:)];
    [active release];
    [inactive release];
    
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end
